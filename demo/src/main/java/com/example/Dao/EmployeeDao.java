package com.example.Dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.Model.Employee;

 
public interface EmployeeDao extends JpaRepository<Employee, Integer> {
	
}
