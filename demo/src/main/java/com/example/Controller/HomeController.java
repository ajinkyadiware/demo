package com.example.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.Dao.EmployeeDao;

import com.example.Model.Employee;

@Controller
@RequestMapping("/getEmployee")
public class HomeController {

	@Autowired
	private EmployeeDao empDao;
	
	@RequestMapping(method=RequestMethod.GET)
	@ResponseBody
	public List<Employee> getEmployee()
	{
		List<Employee> emplist=empDao.findAll();
		  
		return emplist;
		
	}
	
	@RequestMapping(method=RequestMethod.POST)
	@ResponseBody
	public Employee insertEmployee(Employee emp)
	{
		empDao.save(emp);
		  
		return emp;
		
	}
	
	
}
